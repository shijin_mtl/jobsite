export const members = [
    {
        role: "CEO",
        name: "Radu",
        description: `
            CEO in CV30 Company
            cv30 connects talent with career opportunities in the digital age and empowers startups & companies to build their legacy through robust and dynamic partnerships.
        `
    },
    {
        role: "CTO",
        name: "Rares",
        description: `
            CTO in CV30 Company
            cv30 connects talent with career opportunities in the digital age and empowers startups & companies to build their legacy through robust and dynamic partnerships.
        `
    },
    {
        role: "Back-end developer",
        name: "Vlad",
        description: `
            Backend developer in CV30 Company
            cv30 connects talent with career opportunities in the digital age and empowers startups & companies to build their legacy through robust and dynamic partnerships.
        `
    },
    {
        role: "Front-end developer",
        name: "XiaoJin Li",
        description: `
            Frontend developer in CV30 Company
            cv30 connects talent with career opportunities in the digital age and empowers startups & companies to build their legacy through robust and dynamic partnerships.
        `
    },
]

export const descriptions = [
    `
        cv30 emphasises teams and professionals'​ culture and values, easy discoverable and searchable. 
    Our approach makes it more fun, easier and more productive for professionals, teams and companies to focus on productivity and the great products they are working on.
    `,
    `
        We are one future oriented, interactive & trendsetting start-up, revolving around countless personal & professional development ideas, persevering in the authentic interaction between ambitious professionals. 
    Be part of the cv30 network and join us @ cv30.co  
    `
]

export const companyProfile = `
cv30 connects talent with career opportunities in the digital age and empowers startups & companies to build their legacy through robust and dynamic partnerships.

cv30 emphasises teams and professionals'​ culture and values, easy discoverable and searchable. 
Our approach makes it more fun, easier and more productive for professionals, teams and companies to focus on productivity and the great products they are working on.

We are one future oriented, interactive & trendsetting start-up, revolving around countless personal & professional development ideas, persevering in the authentic interaction between ambitious professionals. 
Be part of the cv30 network and join us @ cv30.co  
`