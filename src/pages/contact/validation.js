const yup = require('yup');

module.exports = yup.object().shape({
    name: yup.string().trim().required().max(255),
    email: yup.string().trim().required().max(255),
    message: yup.string().trim().required().max(255),
})