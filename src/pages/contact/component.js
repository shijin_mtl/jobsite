import React from 'react'
import ReCAPTCHA from "react-google-recaptcha";
import Button from '@material-ui/core/Button';
import { FormattedMessage } from 'react-intl'
import CInput from './components/CInput'
import validation from './validation'
class Contact extends React.Component {
    constructor () {
        super ()
        this.state = {
            name: "",
            email: "",
            message: ""
        };        
    }
    validateEmail = (email) => {   
        var re = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        return re.test(email);
    }
    
    handleChange = (name , value) => {
        this.setState({
          [name]: value,
        });
        console.log(this.state)
      };

    onChange = (value) => {
        console.log("Captcha value:", value);
    }

    handleSubmit = () => {
        if ( this.validateEmail(this.state.email) ) {
            validation
            .isValid({
                ...this.state
            })
            .then((valid) => {
                console.log(valid)
            })
        } else {
            console.log("Invalid Email")
        }
    }

    render () { 
        console.log("-------------- Contact ----------------")
        return (
            <div id="contactPage" className='contactPageRoot'>
                <FormattedMessage id="contact.title" defaultMessage="Contact" description="Contact Title Header">
                    {(text) => (
                        <h1>{text}</h1>
                    )}
                </FormattedMessage>
                <div className="contact-form">
                    <div className="name-email">
                        <CInput placeholder="Name" label="Name*" name="name" handleChange={this.handleChange}/>
                        <CInput placeholder="Email" label="Email*" name="email" handleChange={this.handleChange}/>
                    </div>
                    
                    <CInput placeholder="Message" label="Message" name="message" handleChange={this.handleChange}/>
                    <div className="recaptch-submit">
                        <ReCAPTCHA
                            sitekey="6LficHUUAAAAAByU9azezidIYWU5snqn4RMSi-kT"
                            onChange={this.onChange}
                        />
                        <Button fullWidth color="primary" onClick={this.handleSubmit}>Send</Button>
                    </div>
                </div>
                
            </div>
        )
    }
    
}

export default Contact