import React, { Component } from 'react'
import {
    TextField,
} from '@material-ui/core'

class CInput extends Component {
    constructor () {
        super ()
        this.state = {
            value: ""
        }
    }
    handleChange = name => e => {
        const { handleChange } = this.props
        this.setState({
            value: e.target.value
        })
        handleChange(name, e.target.value)
    }
    render () {
        const { name, label } = this.props
        return (
            <div className="cinput-container">
                <TextField
                    fullWidth
                    id={name === "email"? "outlined-email-input" : "outlined-name" }
                    type={name === "email"? "email" : "name"}
                    label={label}
                    value={this.state.value}
                    onChange={this.handleChange(name)}
                    margin="normal"
                    variant="outlined"
                    multiline={name === "message"? true : false}
                    />           
            </div>
        )
    }
}

export default CInput