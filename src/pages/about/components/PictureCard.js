import React, { Component } from 'react'

class PictureCard extends Component {
    render () {
        const { direction, picture, description } = this.props
        return (
            <div className="picture-card-container">
                {
                    direction?
                    <div className="picture-panel">
                        <img src={picture} alt="CV30" />
                    </div>
                    :
                    <div className="description-panel">
                        <p>{description}</p>
                    </div>
                    
                }
                {
                    direction? 
                    <div className="description-panel">
                        <p>{description}</p>
                    </div>
                    :
                    <div className="picture-panel">
                        <img src={picture} alt="CV30" />
                    </div>
                }
            </div>
        )
    }
}

export default PictureCard