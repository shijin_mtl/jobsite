import React, { Component } from 'react'
import AliceCarousel from 'react-alice-carousel';
import { Icon } from '@material-ui/core';

import MemberCard from './MemberCard'
const responsive = {
    0: { items: 1 },
};
class TeamGallery extends Component {

    renderItems = (members) => {
        return members.map(
            ( item, index ) => {
                return <div key={index}><MemberCard item={item} /></div>
            }
        )
    }

    render () {
        const { members } = this.props
        return (
            <div className="team-gallery-container">
                
                <AliceCarousel responsive={responsive} dotsDisabled={true} buttonsDisabled={true} ref={ el => this.Carousel = el }>
                    {this.renderItems(members)}
                </AliceCarousel>
                <div className="arrow-gallery">
                    <Icon className="left-icon" onClick={() => this.Carousel._slidePrev()}>keyboard_arrow_left</Icon>
                    <Icon className="right-icon" onClick={() => this.Carousel._slideNext()}>keyboard_arrow_right</Icon>
                </div>
            </div>
        )
    }
}

export default TeamGallery