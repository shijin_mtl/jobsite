import React, { Component } from 'react'
import AvatarImage from '../../../assets/about/radu.jpg'

class MemberCard extends Component {
    render () {
        const { item: { role, name, description }} = this.props
        return (
            <div className="member-card-container">
                <div className="avatar">
                    <img src={ AvatarImage } alt="Avatar" />
                </div>
                <div className="profile">
                    <h3>{role}</h3>
                    <h1>{name}</h1>
                    <p>{description}</p>
                </div>
            </div>
        )
    }
}

export default MemberCard