import React from 'react'
import TeamGallery from './components/TeamGallery'
import PictureCard from './components/PictureCard'

import { members, descriptions, companyProfile } from '../../constants/about'

import PictureImage1 from '../../assets/about/persons.jpg'
import PictureImage2 from '../../assets/about/persons-2.jpg'

class About extends React.Component {
    
    renderItems = ( pictures, descriptions ) => {
        var direction = true
        return pictures.map(
            (item, index) => {
                if (index % 2 === 1) direction = false
                return <div key={index}><PictureCard description={descriptions[index]} picture={item} direction={direction}/></div>
            }
        )
    }
    render () {
        console.log("-------------- About ----------------")
        const pictures = [ PictureImage1, PictureImage2]

        return (
            <div id="aboutPage" className='aboutPageRoot'>
                {this.renderItems(pictures, descriptions)}
                <TeamGallery members={members} />
                <p className="bottomPanel">{ companyProfile }</p>
            </div>
        )
        
    }
    
}

export default About