import { compose , pure } from 'recompose'
import { withRouter } from 'react-router-dom'
import About from './component'

const AboutHOC = compose (
    withRouter,
    pure
)

export default AboutHOC(About)