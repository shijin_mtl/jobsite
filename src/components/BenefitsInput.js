import React from 'react';
import { compose, pure } from 'recompose';
import { withRouter } from 'react-router-dom';
import { graphql } from 'react-apollo';
import { injectIntl, FormattedMessage } from 'react-intl';

import * as benefits from '../assets/benefits';
import { industriesQuery } from '../store/queries';
import  { AutoSuggestField } from './FormHOCs';

const IndustryInputHOC = compose(
    withRouter,
    graphql(industriesQuery, {
        name: 'industriesQuery',
        options: ({ match}) => ({
            variables: {
                language: match.params.lang,
            },
            fetchPolicy: 'network-only'
        }),
    }),
    injectIntl,
    pure
)

const BenefitsInput = props => {
    const { industriesQuery, selectedBenefits } = props;

    if (industriesQuery.loading || !industriesQuery.industries) {
        return (
            <FormattedMessage id="industry.loading" defaultMessage="Industry..." description="Industry">
                {(text) => (
                    <div>{text}</div>
                )}
            </FormattedMessage> 
        );
    }

    const { intl } = props;

    const suggestions = industriesQuery.industries.map(industry => ({
        value: industry.id,
        label: intl.formatMessage({ id: `industries.${industry.key}` })
    }));
    
    return (
        <Select
            multiple
            value={selectedBenefits}
            onChange={handleChange}
            input={<Input name="jobBenefits" />}
            renderValue={selected => (
                <div className='selectedBenefits'>
                    {selected.map(id => {
                        let benefit = jobBenefits.find(benefit => benefit.id === id);
                        if (benefit)
                            return (
                                <FormattedMessage id={`benefits.${benefit.key}`} defaultMessage={benefit.key} key={benefit.key}>
                                    {(text) => <Chip label={text} className='chip' />}
                                </FormattedMessage>
                            )
                        else
                            return null;
                    })}
                </div>
            )}
            className='jobSelect'
        >
            {jobBenefits.map(benefit => (
                <MenuItem key={benefit.id} value={benefit.id}>
                    <Checkbox checked={selectedBenefits.indexOf(benefit.id) > -1} />
                    <img style={{ marginRight: '10px', width: '20px' }} src={benefits[benefit.key.replace(/\b-([a-z])/g, function(all, char) { return char.toUpperCase() })]} alt={benefit.key} />
                    <FormattedMessage id={`benefits.${benefit.key}`} defaultMessage={benefit.key}>
                        {(text) => <ListItemText primary={text} />}
                    </FormattedMessage>

                </MenuItem>
            ))}
        </Select>
    )
}

export default IndustryInputHOC(IndustryInput);