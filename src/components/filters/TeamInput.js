import React from 'react';
import { compose, pure } from 'recompose';
import { graphql } from 'react-apollo';
import { FormattedMessage } from 'react-intl';

import { teamsSuggestionsQuery } from '../../store/queries';
import  { AutoSuggestField } from '../FormHOCs';

const TeamInputHOC = compose(
    pure,
    graphql(teamsSuggestionsQuery, {
        name: 'teamsSuggestionsQuery',
        options: {
            fetchPolicy: 'network-only'
        },
    })
)

const teamInput = props => {
    const { teamsSuggestionsQuery } = props;

    if (teamsSuggestionsQuery.loading || !teamsSuggestionsQuery.teamsSuggestions) {
        return (
            <FormattedMessage id="team.loading" defaultMessage="Team..." description="Team">
                {(text) => (
                    <div>{text}</div>
                )}
            </FormattedMessage> 
        );
    }

    const suggestions = teamsSuggestionsQuery.teamsSuggestions.map(team => ({
        value: team,
        label: team
    }));
    
    return (
        <AutoSuggestField
            {...props}
            suggestions={suggestions}
            name={props.name || 'team'}
        />
    )
}

export const TeamInput = TeamInputHOC(teamInput);