import React from 'react';
import { TextField, Checkbox } from '@material-ui/core';

export class SalaryFilterInput extends React.PureComponent {
    state = {
        salary: {
            amountMin: 0,
            amountMax: 10000,
            currency: 'eur'
        },
        isEnabled: false
    }
    reportSalary = () => this.props.onChange(this.state.isEnabled ? this.state.salary : undefined)
    handleMinChange = event => this.setState({ salary: { ...this.state.salary, amountMin: parseInt(event.target.value) }}, this.reportSalary)
    handleMaxChange = event => this.setState({ salary: { ...this.state.salary, amountMax: parseInt(event.target.value) }}, this.reportSalary)
    handleIsEnabled = () => this.setState({ isEnabled: !this.state.isEnabled}, this.reportSalary)
    render() {
        const { amountMin, amountMax } = this.state.salary;
        return (
            <div style={{ display: 'flex', flexDirection: 'row', alignItems: 'center' }}>
                <Checkbox
                    style={{
                        color: '#397db9'
                    }}
                    checked={this.state.isEnabled}
                    onChange={this.handleIsEnabled}
                />
                <TextField
                    id="salary-min"
                    placeholder="minimum"
                    value={amountMin}
                    onChange={this.handleMinChange}
                    type="number"
                    className={"textfield"}
                    InputLabelProps={{
                        shrink: true,
                    }}
                    InputProps={{
                        inputProps: {
                            min: 0,
                            max: amountMax
                        }
                    }}
                    style={{ marginTop: 0, marginBottom: 0 }}
                    margin="normal"
                    disabled={!this.state.isEnabled}
                />
                <TextField
                    id="salary-max"
                    placeholder="maximum"
                    value={amountMax}
                    onChange={this.handleMaxChange}
                    type="number"
                    className={"textfield"}
                    InputLabelProps={{
                        shrink: true,
                    }}
                    InputProps={{
                        inputProps: {
                            min: amountMin
                        }
                    }}
                    style={{ marginLeft: 10, marginTop: 0, marginBottom: 0 }}
                    margin="normal"
                    disabled={!this.state.isEnabled}
                />
            </div>
        );
    }
}